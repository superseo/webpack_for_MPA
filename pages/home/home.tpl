<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Подгрузка и шаблонизация JSON</title>
</head>
<body class="image">

<ul class="persons">
 <script id="template" type="text/x-handlebars-template">
{{#each person}}
        <li>
             <h2>{{this.firstName}} {{this.lastName}}, {{this.age}} лет.</h2>
        </li>
{{/each}}
 </script>
</ul>

<button>Get AJAX</button>

</body>
</html>