'use strict';
const webpack = require('webpack');
const path = require('path');

const NODE_ENV = process.env.NODE_ENV || 'development';
const sourcePath = path.join(__dirname, 'pages');
const outputPath = path.join(__dirname, 'build');
const _ = require('lodash');

// Webpack plugins
const LoaderOptionsPlugin = require('webpack/lib/LoaderOptionsPlugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const cssExtractor  = new ExtractTextPlugin('[name]/[name].css', '[name].css');

const HTMLWebpackPlugin = require('html-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const provideImport = new webpack.ProvidePlugin({ _: 'lodash' });


const entry = {
    // main: './main',
    home: './home',
    about: './about',
};
const fs = require('fs');
function generateHtmlPlugins (entryDir) {
    const templateFiles = fs.readdirSync(path.resolve(sourcePath, entryDir));
    return templateFiles.map(item => {
        // Split names and extension
        var parts = item.split('.');
        var name = parts[0] === entryDir ? 'index' : parts[0];
        if (parts[1] === 'tpl') {
            return new HTMLWebpackPlugin({
                filename: path.join(outputPath, `${entryDir}/${name}.html`),
                template: path.join(sourcePath, `${entryDir}/${parts[0]}.tpl`),
                minify: { collapseWhitespace:false }
            });
        }
    });
}

// Call our function on our views directory.
var htmlPlugins = (arg) => {
    let generated = generateHtmlPlugins(arg);
    if (generated&&generated.length) return _.filter(generated, item => { return item; });
};

var htmlExtractor = function(arg = entry) {
    let arr = [];
    _.forIn(arg, (value, name) => arr.push(name));
    return _.map(arr, name => {
        return htmlPlugins(name);
    });
};


module.exports = {
    context: __dirname + '/pages',
    entry,
    watch: NODE_ENV == 'development',
    watchOptions: { aggregateTimeout: 100 },
    devtool: NODE_ENV == 'development' ? '' : null,
    resolve: { alias: { 'modules': __dirname + 'modules' } },
    output: {
        path: outputPath, //абсолютный путь к директории сборки
        filename: '[name]/[name].js', // шаблон вместо [name] при сборке будут подставлены home.js и about.js
        library:  '[name]', // аналогично: глобальные переменные home н about
        //publicPath (см. ниже)
    },
    plugins: [
        cssExtractor,
        provideImport,
        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.optimize.CommonsChunkPlugin({
            names: ['common', 'libraries'],
            minChunks: 2
        }),
        new UglifyJsPlugin({ uglifyOptions: { ecma: 5 } }),
        new LoaderOptionsPlugin({ options: { jshint: { esversion: 6 } } }),
        // new HTMLWebpackPlugin({
        //     filename: `home/index.html`,
        //     template: path.join(sourcePath, `home/home.tpl`),
        // }),
    ].concat(...htmlExtractor()),
    module: {
        rules: [{
                enforce: 'pre',
                test: /\.js$/, // запустим загрузчик во всех файлах .js
                exclude: /node_modules/, // проигнорируем все файлы в папке  node_modules 
                use: 'jshint-loader',
            },{
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                include: __dirname,
                query: { cacheDirectory: true }
            },{
                test : /\.css$/,
                exclude: /node_modules/,
                use: ['style-loader','css-loader']
            },{
                test : /\.scss$/,
                exclude: /node_modules/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [/*'style-loader',*/'css-loader','sass-loader']
                })
            },{
                test: /\.html$/,
                // loader: 'raw-loader',
                use: {
                    loader: 'html-loader',
                    query: { minify: true }
                },
            },{
                test: /\.tpl$/,
                exclude: /node_modules/,
                // use: ['html-loader', 'handlebars-loader'],
                loader: 'handlebars-loader',
            },{
              test: /\.(jpg|jpeg|png|svg|gif)$/,
              loader: 'file-loader',
              options: { name: '/images/[name].[ext]' },
            },
        ],
        noParse: [
            /angular\/angular.js/,
            /angular-ui-router/,
            /jquery\/src\/jquery.js/,
            /moment\/moment.js/
        ]
    },
};

// console.log('module.exports',module.exports);